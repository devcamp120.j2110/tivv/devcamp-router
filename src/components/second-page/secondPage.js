import { useEffect } from 'react';
import { useNavigate, useParams } from 'react-router-dom'
function SecondPage() {
    const {id} = useParams();
    const navigate = useNavigate();
    useEffect(()=> {
        if(id==="third") {
            navigate("/thirdpage");
        }
    }, []);
    const goToHome = () => {
        navigate("/");
    }
    return(
        <div>
            {id ? <div>Second Page With ID: {id}</div> : <div>Second Page</div>}
            <button onClick={goToHome}>Home</button>
        </div>
    )
}
export default SecondPage;