import { Route, Routes } from "react-router-dom";
import FirstPage from "./components/first-page/firstPage";
import HomePage from "./components/homepage/homepage";
import SecondPage from "./components/second-page/secondPage";
import ThirdPage from "./components/third-page/thirdPage";


function App() {
  return (
    <div >
      Welcome to Devcamp Router
      <hr />
      <ul>
        <li><a href="/">Home</a></li>
        <li><a href="/firstpage">First Page</a></li>
        <li><a href="/secondpage">Second Page</a></li>
        <li><a href="/secondpage/123">Second Page With ID</a></li>
        <li><a href="/thirdpage">Third Page</a></li>
      </ul>
      <Routes>
        <Route path="/" element={<HomePage />}></Route>
        <Route path="/firstpage" element={<FirstPage />}></Route>
        <Route path="/secondpage" element={<SecondPage />}></Route>
        <Route path="/secondpage" element={<SecondPage />}></Route>
        <Route path="/secondpage/:id" element={<SecondPage />}></Route>
        <Route path="/thirdpage" element={<ThirdPage />}></Route>
        <Route path="*" element={<ThirdPage />}></Route>
      </Routes>
    </div>
  );
}

export default App;
